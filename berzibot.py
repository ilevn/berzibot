import asyncio
import re
import sys
import traceback
from copy import copy

import aiohttp
import asyncpg
import discord
from discord.ext.commands import (
    Bot,
    CheckFailure,
    CommandInvokeError,
    NotOwner,
    UserInputError,
)

from cogs.totd_parts.secrets import (
    DB_TYPE,
    DB_USER,
    DB_PASSWORD,
    DB_HOST,
    DB_PORT,
    DB_NAME,
)
from config import TOKEN, RECENT_COMMAND_TIME
from context import BerziContext
from expiring_cache import ExpiringCache
from locale import fetch_line
from log import logger

DB_STRING = (
    f"{DB_TYPE}://"
    f"{DB_USER}{':' + DB_PASSWORD if DB_PASSWORD else ''}"
    f"@{DB_HOST}{':' + DB_PORT if DB_PORT else ''}"
    f"{'/' + DB_NAME if DB_NAME else ''}"
)


class Berzibot(Bot):
    default_activity = discord.Activity(
        type=discord.ActivityType.listening, name=fetch_line("listening to")
    )

    _sleeping = False
    _sending_errors = False

    def __init__(self, command_prefix, **options):
        super().__init__(command_prefix, **options)
        self.pool = None
        self.session = None
        self.command_keywords = {
            "and": self._new_command_shortcut,
            "again with": self._command_with_new_args,
            "again": self._repeat_command,
        }
        self.keyword_matcher = re.compile(r"^(?P<kw>again with|and|again)")
        self._latest_commands = ExpiringCache(RECENT_COMMAND_TIME)
        """Dict of callers to a tuple of the invoked context and monotonic time of
        invocation."""

    @property
    def sleeping(self):
        return self._sleeping

    @property
    def sending_errors(self):
        return self._sending_errors

    async def get_context(self, message: discord.Message, *, cls=BerziContext):
        return await super().get_context(message=message, cls=cls)

    async def set_sleeping(self, value: bool):
        if value is True:
            self._sleeping = True
            activity = discord.Game(fetch_line("playing dead"))
            await self.change_presence(
                activity=activity, status=discord.Status.idle, afk=True
            )
        elif value is False:
            self._sleeping = False
            await self.change_presence(
                activity=self.default_activity, status=discord.Status.online, afk=False
            )
        else:
            raise ValueError

    def set_sending_errors(self, value: bool):
        if isinstance(value, bool):
            self._sending_errors = value
        else:
            raise ValueError

    async def _new_command_shortcut(self, message: discord.Message, ctx: BerziContext):
        new_message = copy(message)
        # Re-use old prefix.
        new_message.content = ctx.prefix + message.content.lstrip(" and")

        await self.process_commands(new_message)

    async def _command_with_new_args(self, message: discord.Message, ctx: BerziContext):
        # Use original message content to preserve command arg integrity
        # (case sensitive commands, etc).
        new_args = message.content.lstrip("again with")
        new_message = copy(message)

        # Hand command fragment down the context chain.
        new_content = f"{ctx.prefix}{ctx.command.qualified_name} {new_args}"
        new_message.content = new_content

        await self.process_commands(new_message)

    async def _repeat_command(self, _: discord.Message, ctx: BerziContext):
        new_message = copy(ctx.message)
        await self.process_commands(new_message)

    async def process_commands(self, message: discord.Message):
        # Remove excessive spaces to make faulty-formatted commands work.
        # Note: this disallows multiple spaces in argument contents.
        message.content = re.sub(r" +", " ", message.content)

        ctx = await self.get_context(message)

        if ctx.command is None:
            return

        if self.sleeping and not await self.is_owner(message.author):
            await ctx.send(fetch_line("sleeping"))
            return

        try:
            async with ctx.acquire():
                await self.invoke(ctx)
        except Exception:
            raise
        else:
            self._latest_commands[message.author.id] = ctx

    async def on_ready(self):
        self.pool = await asyncpg.create_pool(DB_STRING)
        self.session = aiohttp.ClientSession()

        await self.change_presence(
            activity=self.default_activity, status=discord.Status.online, afk=False
        )

    async def on_command_error(self, ctx, error):
        if isinstance(error, CommandInvokeError):
            err = error.original
            exc_info = (type(err), err, err.__traceback__)
            exc = "".join(traceback.format_exception(*exc_info))
            logger.error(exc)
            print(exc)

        if isinstance(error, NotOwner):
            await ctx.send(fetch_line("only owner"))
            return

        if isinstance(error, CheckFailure):
            await ctx.send(fetch_line("insufficient permissions"))
            return

        if isinstance(error, UserInputError):
            await ctx.send(fetch_line("user error"))
            return

        await ctx.send(fetch_line("fail"))

    async def on_error(self, event, *args, **kwargs):
        exception_info = sys.exc_info()
        if not isinstance(exception_info[0], asyncio.exceptions.TimeoutError):
            traceback.print_exc()
            logger.exception(exception_info)
            if self.sending_errors:
                owner = self.get_user(self.owner_id)
                await owner.send(exception_info)

    async def on_message(self, message):
        if message.author.bot:
            return

        if message.content.startswith(tuple(self.command_keywords)):
            if old_ctx := self._latest_commands.fetch(message.author.id):
                keyword = self.keyword_matcher.match(message.content).group("kw")
                if keyword:
                    func = self.command_keywords[keyword]
                    await func(message, old_ctx)
                    return

        await self.process_commands(message)


berzibot = Berzibot(discord.ext.commands.bot.when_mentioned)

if __name__ == "__main__":
    berzibot.remove_command("help")
    berzibot.load_extension("cogs.basic_functionality")
    berzibot.load_extension("cogs.chat_control")
    berzibot.load_extension("cogs.thing_of_the_day")
    berzibot.run(TOKEN)
