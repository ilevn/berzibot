from copy import copy

from discord.ext.commands import Command, Group, command


class HasMeta:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._meta = copy(kwargs.get("meta", {}))
        if not isinstance(self._meta, dict):
            raise ValueError

    def get_meta(self, key: str):
        return self._meta.get(key, None)


class BerziCommand(HasMeta, Command):
    """Command class able to store arbitrary attributes in the meta property."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


def berzi_command(name=None, cls=BerziCommand, **attrs):
    """Transforms a function into a :class:`BerziCommand`."""

    def decorator(func):
        if isinstance(func, Command):
            raise TypeError("Callback is already a command.")
        return cls(func, name=name, **attrs)

    return decorator


class BerziGroup(HasMeta, Group):
    """Group class able to store arbitrary attributes in the meta property."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


def berzi_group(name=None, **attrs):
    """Transforms a function into a :class:`BerziGroup`."""
    attrs.setdefault("cls", BerziGroup)
    return command(name=name, **attrs)
