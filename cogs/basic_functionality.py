import io
import time
import traceback
from discord.ext.commands import (
    Cog,
    is_owner,
    group,
    command,
    ExtensionNotLoaded,
    ExtensionAlreadyLoaded,
    ExtensionNotFound,
    NoEntryPointError,
    ExtensionFailed,
)
from berzibot import Berzibot
from berzicommand import BerziCommand, BerziGroup
from config import DEFAULT_DELETE_AFTER
from context import BerziContext as Context
from log import logger
from locale import fetch_line
from pathlib import Path
from typing import Literal, Tuple
from datetime import datetime
from utils import to_snake_case, EmbedPaginator, from_snake_case
import discord


def setup(bot: Berzibot):
    bot.add_cog(BasicFunctionality(bot))


class BasicFunctionality(Cog, command_attrs=dict(hidden=True)):
    """Hold all basic functionality."""

    def __init__(self, bot: Berzibot):
        self.bot = bot

    @Cog.listener()
    async def on_ready(self):
        logger.info("Connected.")

    @Cog.listener()
    async def on_disconnect(self):
        logger.info("Disconnected.")

    @command(name="help", hidden=False, cls=BerziCommand)
    async def help(self, ctx: Context, *, for_command: str = None):
        """Display commands and their usage."""
        if for_command:
            for_command = for_command.lower().strip()

        embed = discord.Embed()
        embed.description = fetch_line("instructions")
        embed.set_thumbnail(url=self.bot.user.avatar_url)
        embed.colour = self.bot.user.colour
        embed.title = for_command or "Help"

        no_help = fetch_line("no help")

        fields = []
        for cmd in self.bot.walk_commands():
            if for_command and cmd.name != for_command:
                continue

            if not for_command and (
                cmd.hidden or isinstance(cmd, discord.ext.commands.Group)
            ):
                continue

            # Hide mod-only commands from users who don't have appropriate permissions.
            if (
                not await self.bot.is_owner(ctx.author)
                and (
                    cmd.get_meta("mod_only")
                    and not ctx.author.guild_permissions.manage_channels
                )
                or (
                    cmd.get_meta("admin_only")
                    and not ctx.author.guild_permissions.administrator
                )
            ):
                continue

            signature = cmd.signature or ""
            signature = signature.translate(str.maketrans("[]", "()", "<>"))
            name = (
                f"{'🚫 ' if not cmd.enabled else ''}"
                f"{cmd.qualified_name}{' `' + signature + '`' if signature else ''}"
            )

            help_text = (cmd.help if for_command else cmd.short_doc) or no_help

            fields.append((name, help_text))

        if not fields:
            await ctx.send(fetch_line("fail find", for_command))
            return

        # TODO divide in groups

        paginator = EmbedPaginator(
            ctx,
            fields,
            thumbnail=self.bot.user.avatar_url,
            title=f"Help for {for_command if for_command else 'my commands'}",
            description="Ping me with a command!",
        )
        await paginator.paginate()

    @command(name="ping", hidden=False, cls=BerziCommand)
    async def ping(self, ctx: Context):
        """Ping the bot to check response time."""
        delay = (ctx.message.created_at - datetime.now()).microseconds
        await ctx.send(f"Pong! 🏓 {delay:.3f}μs.")

    @is_owner()
    @command(name="die", hidden=True, cls=BerziCommand)
    async def die(self, ctx: Context):
        """Kill me. :("""
        await ctx.send(fetch_line("logout"))
        await self.bot.logout()

    @is_owner()
    @command(name="sleep", hidden=True, cls=BerziCommand)
    async def sleep(self, ctx: Context):
        """Go to sleep, ignoring all commands except the wake up call."""
        # TODO make guild-wide usable by admins
        if not self.bot.sleeping:
            await self.bot.set_sleeping(True)
            await ctx.send(fetch_line("goodnight"))

    @is_owner()
    @command(name="wake", hidden=True, cls=BerziCommand)
    async def wake(self, ctx: Context, up: str = None):
        """Wake up after being put to sleep."""
        if self.bot.sleeping:
            # Memeily force correct English, lul.
            if up.lower().strip() == "up":
                await self.bot.set_sleeping(False)
                await ctx.send(fetch_line("wake up"))

    @is_owner()
    @command(name="echo", hidden=True, cls=BerziCommand)
    async def echo(self, ctx: Context, first: str = None, *, other: str = None):
        await ctx.send(first)
        if other:
            await ctx.send(other)

    @is_owner()
    @group(name="errors", aliases=("error",), hidden=True, cls=BerziGroup)
    async def errors(self, ctx: Context):
        """Commands for error reporting and handling."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "errors"))

    @is_owner()
    @errors.command(name="send", hidden=True, cls=BerziCommand)
    async def errors_send(self, ctx: Context):
        currently_sending = self.bot.sending_errors
        self.bot.set_sending_errors(not currently_sending)
        await ctx.send(
            f"Ok,{'' if currently_sending else ' not'} sending errors from now on.",
            delete_after=DEFAULT_DELETE_AFTER,
        )

    @is_owner()
    @errors.command(name="logs", aliases=("log",), hidden=True, cls=BerziCommand)
    async def errors_log(self, ctx: Context):
        try:
            with open("../discord.log", "r") as fd:
                message = fd.read()
        except FileNotFoundError:
            message = "Couldn't find the file!"

        await ctx.author.send(message)

    @is_owner()
    @group(name="cog", hidden=True, cls=BerziGroup)
    async def cog(self, ctx: Context):
        """Commands for cog administration."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "cog"))

    def _list_cogs(self) -> Tuple[set, set]:
        """Return a set of already loaded cogs and one of available cogs."""
        already_loaded = {to_snake_case(cog_name) for cog_name in self.bot.cogs.keys()}

        available = {
            cog.name[:-3]
            for cog in Path("cogs/").glob("*.py")
            if not cog.name.startswith("_")
        }

        return already_loaded, available

    async def _manage_cogs(
        self,
        ctx: Context,
        cog_names: str,
        operation: Literal["load", "unload", "reload"],
    ):
        operation = operation.lower()
        if operation not in ("load", "unload", "reload"):
            raise ValueError(f"Unknown operation: {operation}.")

        already_loaded, available = self._list_cogs()
        this_cog = {__name__.split(".")[-1]}

        if cog_names.lower() == "all":
            to_process = available
        else:
            cog_names = cog_names.split(",")
            to_process = {to_snake_case(name) for name in cog_names}

        if operation == "load":
            to_process = to_process.difference(already_loaded)
        elif operation == "reload":
            to_process = to_process.intersection(already_loaded)
        elif operation == "unload":
            to_process = to_process.intersection(already_loaded).difference(this_cog)

        if not to_process:
            await ctx.send(fetch_line("successful"))
            return

        processed = 0
        not_found = 0
        for cog_name in to_process:
            try:
                getattr(self.bot, f"{operation}_extension")(f"cogs.{cog_name}")
                logger.info(f"{operation.capitalize()}ed cog {cog_name}.")
                processed += 1
            except (ExtensionAlreadyLoaded, NoEntryPointError, ExtensionFailed) as e:
                logger.info(f"Failed to load {cog_name=}. {e}")
            except (ExtensionNotFound, ExtensionNotLoaded) as e:
                not_found += 1
                logger.info(f"Failed to find {cog_name=}. {e}")

        if processed:
            await ctx.send(fetch_line("successful"))
        else:
            await ctx.send(fetch_line("fail"))

        if not_found:
            await ctx.send(fetch_line("fail find", f"{not_found} of them"))

    @cog.command(name="load", hidden=True, cls=BerziCommand)
    async def load(self, ctx: Context, *, cog_names: str):
        """Load a cog."""
        await self._manage_cogs(ctx, cog_names, "load")

    @cog.command(name="unload", hidden=True, cls=BerziCommand)
    async def unload(self, ctx: Context, *, cog_names: str):
        """Unload a cog."""
        await self._manage_cogs(ctx, cog_names, "unload")

    @cog.command(name="reload", hidden=True, cls=BerziCommand)
    async def reload(self, ctx: Context, *, cog_names: str):
        """Reload a cog."""
        await self._manage_cogs(ctx, cog_names, "reload")

    @cog.command(name="list", hidden=True, cls=BerziCommand)
    async def list(self, ctx: Context):
        """List all available and loaded cogs."""
        loaded, available = self._list_cogs()

        def format_cog(cog_name: str) -> str:
            icon = "🏳️" if cog_name in loaded else "🏴"
            cog_name = from_snake_case(cog_name)

            return f"{icon} {cog_name}"

        message = f"Available cogs:\n"
        message += "\n".join((format_cog(cog) for cog in available))

        await ctx.send(message)

    # Sql debug commands

    class TabularData:
        def __init__(self):
            self._widths = []
            self._columns = []
            self._rows = []

        def set_columns(self, columns):
            self._columns = columns
            self._widths = [len(c) + 2 for c in columns]

        def add_row(self, row):
            rows = [str(r) for r in row]
            self._rows.append(rows)
            for index, element in enumerate(rows):
                width = len(element) + 2
                if width > self._widths[index]:
                    self._widths[index] = width

        def add_rows(self, rows):
            for row in rows:
                self.add_row(row)

        def render(self):
            """
            Example:
            +-------+-----+
            | Memes  | Ye |
            +-------+-----+
            | Alice | 24  |
            |  Bob  | 19  |
            +-------+-----+
            """

            sep = "+".join("-" * w for w in self._widths)
            sep = f"+{sep}+"

            to_draw = [sep]

            def get_entry(d):
                elem = "|".join(f"{e:^{self._widths[i]}}" for i, e in enumerate(d))
                return f"|{elem}|"

            to_draw.append(get_entry(self._columns))
            to_draw.append(sep)

            for row in self._rows:
                to_draw.append(get_entry(row))

            to_draw.append(sep)
            return "\n".join(to_draw)

    @staticmethod
    def cleanup_code(content):
        """Remove code blocks"""
        # remove ```py\n```
        if content.startswith("```") and content.endswith("```"):
            return "\n".join(content.split("\n")[1:-1])

        # remove `foo`
        return content.strip("` \n")

    @staticmethod
    def format_table(results):
        table = BasicFunctionality.TabularData()
        table.set_columns(list(results[0].keys()))
        table.add_rows(list(r.values()) for r in results)
        return table.render()

    @is_owner()
    @command(hidden=True, cls=BerziCommand)
    async def sql(self, ctx, *, query: str):
        """Run some SQL."""

        query = self.cleanup_code(query)

        is_multistatement = query.count(";") > 1
        if is_multistatement:
            # fetch does not support multiple statements
            strategy = ctx.db.execute
        else:
            strategy = ctx.db.fetch

        # noinspection PyBroadException
        try:
            start = time.perf_counter()
            results = await strategy(query)
            dt = (time.perf_counter() - start) * 1000.0
        except Exception:
            return await ctx.send(f"```py\n{traceback.format_exc()}\n```")

        rows = len(results)
        if is_multistatement or rows == 0:
            return await ctx.send(f"`{dt:.2f}ms: {results}`")

        render = self.format_table(results)

        fmt = f"```\n{render}\n```\n*Returned row(s) in {dt:.2f}ms*"
        if len(fmt) > 2000:
            async with ctx.bot.session.post(
                "http://0x0.st", data={"file": io.StringIO(fmt)}
            ) as r:
                await ctx.send(f"Too many results: {await r.text()}")
                return

        await ctx.send(fmt)
