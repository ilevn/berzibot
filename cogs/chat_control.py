from discord.ext.commands import (
    Cog,
    group,
    command,
    has_permissions,
    CheckFailure,
)
import discord
from berzibot import Berzibot
from typing import Optional, Union
from berzicommand import BerziCommand, BerziGroup
from log import logger
from config import DEFAULT_DELETE_AFTER
from context import BerziContext as Context
from locale import fetch_line


def setup(bot: Berzibot):
    bot.add_cog(ChatControl())


class ChatControl(Cog):
    """Hold all commands for controlling chat and channels."""

    @has_permissions(manage_messages=True)
    @command(name="embed", cls=BerziCommand, meta={"mod_only": True})
    async def embed(self, ctx: Context, title: str, *, content: str):
        """Embed some text for better visibility.
        
        :param ctx: the context.
        :param title: the title for the embed, within quotes if more than one word.
        :param content: the text to embed.
        """

        embed = discord.Embed()

        quotes = "'\""
        content = (content or "").strip(quotes)
        title = (title or "").strip(quotes)

        if not (content and title):
            await ctx.send(fetch_line("user error"))
            return

        if not (len(content) <= 1024 and len(title) <= 256) or len(embed) > 6000:
            await ctx.send(fetch_line("too long"))
            return

        embed.add_field(name=title, value=content, inline=False)

        await ctx.message.delete()
        await ctx.send(embed=embed)

    @group(name="channel", cls=BerziGroup, meta={"mod_only": True})
    async def channel(self, ctx: Context):
        """Commands for text channel administration."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "channel"))

    @has_permissions(manage_messages=True, read_message_history=True)
    @channel.command(name="flush", cls=BerziCommand, meta={"mod_only": True})
    async def flush(
        self,
        ctx: Context,
        channel: Optional[discord.TextChannel],
        member: Optional[Union[discord.Member, discord.User]],
    ):
        """Remove all messages from a channel, optionally only by a specific user."""
        # Allow specifying a member without a channel:
        if not member and (
            isinstance(channel, discord.Member) or isinstance(channel, discord.User)
        ):
            member = channel
            channel = None

        if (
            member
            and not member.bot
            and member.guild_permissions <= ctx.author.guild_permissions
        ):
            raise CheckFailure

        channel = channel or ctx.channel

        def check(message: discord.Message) -> bool:
            return message.author == member

        async with ctx.typing():
            await channel.purge(check=check if member else None)
        await ctx.send(fetch_line("successful"), delete_after=DEFAULT_DELETE_AFTER)

        for_member = f" for {member=}" if member else ""
        logger.info(
            f"{ctx.author} flushed {channel.name=} on {ctx.guild.name=}{for_member}."
        )

    @has_permissions(manage_messages=True, read_message_history=True)
    @channel.command(name="truncate", cls=BerziCommand, meta={"mod_only": True})
    async def truncate(
        self, ctx: Context, direction: str, start: discord.Message,
    ):
        """Truncate all messages before or after a certain message (included).
        
        :param ctx: the context.
        :param direction: either `before` or `after` the given start point.
        :param start: the message from which to start truncating.
        """

        direction = direction.lower()

        if direction not in ("before", "after"):
            raise discord.InvalidArgument

        def check(message: discord.Message) -> bool:
            return (
                message.created_at <= start.created_at
                if direction == "before"
                else message.created_at >= start.created_at
            )

        channel = ctx.channel

        async with ctx.typing():
            await channel.purge(check=check)
        await ctx.send(fetch_line("successful"), delete_after=DEFAULT_DELETE_AFTER)

        logger.info(
            f"{ctx.author} truncated {channel.name=} on {ctx.guild.name=} "
            f"{direction} {start.created_at}."
        )
