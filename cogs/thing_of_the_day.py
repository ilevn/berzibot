import asyncio
from typing import Optional, Tuple, Union
import discord
from discord.ext import tasks
from discord.ext.commands import (
    Cog,
    group,
    has_permissions,
    is_owner,
    clean_content,
    UserInputError,
)
import utils
from berzibot import Berzibot
from berzicommand import BerziGroup, BerziCommand
from context import BerziContext as Context
from log import logger
from locale import fetch_line
from functools import reduce
import string


punctuation = set(string.punctuation).difference(set("[]{}()<>"))
"""Punctuation characters except parentheses."""


def setup(bot: Berzibot):
    bot.add_cog(ThingOfTheDay(bot))


class ThingFormatter:
    __slots__ = []

    @staticmethod
    def format_entry(
        qualifier_string: str, content_string: str, extra_string: str
    ) -> str:
        """Format a message to publish an entry given its parts."""
        if not qualifier_string.isupper():
            qualifier_string = qualifier_string.title() or "Word"

        if extra_string and extra_string[-1] not in punctuation:
            extra_string += "."

        return (
            f"{qualifier_string} of the day: "
            f"**{content_string}**"
            f"{': ' + extra_string if extra_string else '.'}\n\n"
            f"{fetch_line('totd footer')}"
        )

    @staticmethod
    def embed_entry(
        qualifier_string: str,
        content_string: str,
        extra_string: str,
        guild: discord.Guild,
    ) -> discord.Embed:
        embed = discord.Embed()

        # Keep acronyms as-is, force title case otherwise.
        if not qualifier_string.isupper():
            qualifier_string = qualifier_string.title() or "Word"
        embed.title = f"{qualifier_string} of the day"

        embed.set_thumbnail(url=guild.icon_url)

        if extra_string:
            if extra_string[-1] not in punctuation:
                extra_string += "."

            extra_string = f"*{extra_string}*"
        if not extra_string:
            extra_string = "\N{ZERO WIDTH SPACE}"  # Circumvent empty field error.
        embed.add_field(
            name=f"> **{content_string}**", value=f"{extra_string}", inline=False
        )

        embed.set_footer(
            text=fetch_line("totd footer"),
            # Information icon from twitter's emoji.
            icon_url="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com"
            "/thumbs/120/twitter/233/information-source_2139.png",
        )

        return embed


class ThingOfTheDay(Cog):
    """Hold all the commands for thing of the day services."""

    def __init__(self, bot: Berzibot):
        self.bot = bot
        self.publish_totd.add_exception_type(Exception)
        self.publish_totd.add_exception_type(BaseException)
        self.publish_totd.start()

    def cog_unload(self):
        self.publish_totd.cancel()

    @staticmethod
    def _build_query(ctx: Context, **kwargs) -> Tuple[str, list]:
        """Assemble a WHERE clause for a query with the provided columns and values.

        Context is used to add the server (guild) to the query.
        Argument names remain unchanged, thus they are case-sensitive.
        "language" is automatically lowercased.
        "channel" and other Discord entities with an id should be provided as instances
        of said entities, the id is automatically extracted.

        :return: a tuple of the query string and a list of the respective values.
        """

        query_names = ["server"]
        query_values = [ctx.guild.id]

        mapping = {
            "channel": lambda x: x.id,
            "author": lambda x: x.id,
            "language": lambda x: x.lower(),
        }

        for column, value in kwargs.items():
            if not value:
                continue

            query_names.append(column)
            transformer = mapping.get(column, lambda x: x)
            query_values.append(transformer(value))

        query_string = " AND ".join(
            f"{name}=${i}" for i, name in enumerate(query_names, 1)
        )

        return query_string, query_values

    # noinspection PyCallingNonCallable
    @tasks.loop(minutes=1.0)
    async def publish_totd(self):
        """Publish all Things of the Day scheduled for the current minute, if any."""
        current_timeofday = utils.current_timeofday()

        async with self.bot.pool.acquire() as conn:
            entries = await conn.fetch(
                "SELECT DISTINCT ON (e.language_id)"
                " e.id,"
                " qualifier,"
                " content,"
                " extra,"
                " t.server,"
                " t.channel,"
                " (SELECT COUNT(*) FROM totd_mentions m"
                " WHERE m.entry_id=e.id"
                " AND m.channel=t.channel"
                " AND m.server=t.server)"
                " AS appeared"
                " FROM totd_entries e JOIN totd_tasks t ON t.language_id=e.language_id"
                " WHERE is_active=true AND send_at=$1"
                " ORDER BY e.language_id, appeared ASC, random();",
                current_timeofday,
            )

            for entry_id, qualifier, content, extra, server, channel in entries:
                await conn.execute(
                    "INSERT INTO totd_mentions"
                    " (entry_id, channel, server)"
                    " VALUES($1, $2, $3);",
                    entry_id,
                    channel,
                    server,
                )

                guild = self.bot.get_guild(server)
                location = guild.get_channel(channel)

                embed = ThingFormatter.embed_entry(qualifier, content, extra, guild)
                embed.colour = self.bot.user.colour

                await location.send(embed=embed)

                logger.info(f"Published {entry_id=} to {channel=} {server=}.")

    @publish_totd.before_loop
    async def check_ready(self):
        await self.bot.wait_until_ready()

    @group(name="totd", hidden=True, cls=BerziGroup, meta={"mod_only": True})
    async def thing_of_the_day(self, ctx):
        """Commands for the "thing of the day" service."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd"))

    @thing_of_the_day.command(name="sample", cls=BerziCommand, meta={"mod_only": True})
    async def sample(
        self, ctx: Context, language: int = None, format_type: str = "embed",
    ):
        """Sample a random entry. Format type can be `embed` or `text`."""
        if format_type not in ("text", "embed"):
            raise UserInputError

        subquery = " WHERE language_id=$1" if language is not None else ""
        arguments = [language] if language is not None else []

        async with self.bot.pool.acquire() as conn:
            entry = await conn.fetch(
                f"SELECT"
                f" qualifier,"
                f" content,"
                f" extra"
                f" FROM totd_entries{subquery}"
                f" ORDER BY random()"
                f" LIMIT 1;",
                *arguments,
            )

        for qualifier, content, extra in entry:
            guild = self.bot.get_guild(ctx.guild.id)
            location = guild.get_channel(ctx.channel.id)

            if format_type == "text":
                text = ThingFormatter.format_entry(qualifier, content, extra)
                await location.send(text)
            else:
                embed = ThingFormatter.embed_entry(qualifier, content, extra, guild)
                await location.send(embed=embed)

    @has_permissions(manage_channels=True)
    @thing_of_the_day.command(name="setup", cls=BerziCommand, meta={"mod_only": True})
    async def setup(
        self,
        ctx,
        language: clean_content,
        channel: Union[discord.TextChannel, str],
        # format_type: str,  # TODO
        *,
        send_at: utils.time_converter = None,
    ):
        """Set up a task for issuing things of the day in a channel.

        :param ctx: the current context.
        :param language: the language to set up the task for. If a nonexistent one is
                          entered, creation is prompted.
        :param channel: the channel in which to set up the task. If "here", use
                         the current channel.
        :param send_at: the time of day at which to send the next issue.
        """

        send_at = send_at or utils.current_timeofday()
        channel = ctx.channel if channel == "here" else channel

        # Fetch the language
        language = language.lower()
        language_id = await ctx.db.fetchval(
            "SELECT id FROM totd_languages WHERE name=$1;", language
        )

        # If not exists: ask confirmation to create (if owner)
        new_language = False
        if not language_id:
            if not await self.bot.is_owner(ctx.author):
                await ctx.send(fetch_line("unknown", language.capitalize()))
                return

            # FIXME with "again with" italian becomes Lian
            message = fetch_line("unknown should", language.capitalize())
            async with ctx.ask(message, boolean=True) as confirm:
                if not confirm:
                    await ctx.send(fetch_line("cancelled"))
                    return

            await ctx.db.execute(
                "INSERT INTO totd_languages (name) VALUES($1);", language
            )

            new_language = True

            await ctx.send(fetch_line("successful"))
            logger.info(
                f"New {language=} created by {ctx.author} "
                f"in {channel=} {ctx.guild=}."
            )

        # Check whether language is already set up here
        if not new_language:
            query = "SELECT id, send_at, is_active FROM totd_tasks WHERE language_id=$1 AND channel=$2 AND server=$3"
            existing_tasks = await ctx.db.fetch(
                query, language_id, channel.id, ctx.guild.id
            )

            if existing_tasks:
                for task in existing_tasks:
                    if task["send_at"] == send_at:
                        # If set up but inactive, prompt to activate
                        if task["is_active"] is True:
                            await ctx.send(fetch_line("task active"))
                            return

                        message = fetch_line("task at for", send_at, language)
                        async with ctx.ask(message, boolean=True) as confirm:
                            if not confirm:
                                await ctx.send(fetch_line("cancelled"))
                                return

                            await ctx.db.execute(
                                "UPDATE totd_tasks SET is_active=true WHERE id=$1;",
                                task["id"],
                            )
                            logger.info(
                                f"Task {task['id']} for language {language}"
                                f" enabled by {ctx.author} "
                                f"in {channel}@{ctx.guild}."
                            )
                            await ctx.send(fetch_line("successful"))

        # Set up task here.
        await ctx.db.execute(
            "INSERT INTO totd_tasks (language_id, send_at, channel, server) VALUES($1, $2, $3, $4);",
            language_id,
            send_at,
            channel.id,
            ctx.guild.id,
        )

        await ctx.send(fetch_line("successful"))
        logger.info(
            f"Task for {language=} set up by {ctx.author=} in {channel=}@{ctx.guild=} for {send_at=}."
        )

    async def _toggle_task_for_language(
        self, ctx, enable: bool, *, language, channel, send_at
    ):
        query_string, query_values = self._build_query(
            ctx, language=language, channel=channel, send_at=send_at
        )
        query = f"UPDATE totd_tasks SET is_active=$1 WHERE {query_string};"

        count = await ctx.db.execute(query, *query_values)[-1]

        mode = "Enabled" if enable else "Disabled"
        await ctx.send(
            f"{mode} {count} task{'s' if count != 1 else ''}!"
            f" {':D' if count > 0 else ':0'}"
        )

        if count:
            logger.info(
                f"{ctx.author}"
                f" {mode.lower()} {count} task{'s' if count != 1 else ''}"
                f" with {query_string=} {query_values=}."
            )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.command(name="enable", cls=BerziCommand, meta={"mod_only": True})
    async def enable(
        self,
        ctx,
        language: str,
        send_at: Optional[utils.time_converter] = None,
        *,
        channel: discord.TextChannel = None,
    ):
        """Enable one or more tasks for the specified language, time, or channel.

        :param ctx: the context.
        :param language: language to narrow down the operation.
        :param send_at: time of day of the task.
        :param channel: channel to narrow down the operation.
        """

        await self._toggle_task_for_language(
            ctx, True, language=language, channel=channel, send_at=send_at
        )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.command(name="disable", cls=BerziCommand, meta={"mod_only": True})
    async def disable(
        self,
        ctx,
        language: str,
        send_at: Optional[utils.time_converter] = None,
        *,
        channel: discord.TextChannel = None,
    ):
        """Disable one or more tasks for the specified language, time, or channel.

        :param ctx: the context.
        :param language: language to narrow down the operation.
        :param send_at: time of day of the task.
        :param channel: channel to narrow down the operation.
        """

        await self._toggle_task_for_language(
            ctx, False, language=language, channel=channel, send_at=send_at
        )

    @has_permissions(manage_channels=True)
    @thing_of_the_day.group(name="list", hidden=True, cls=BerziGroup)
    async def list(self, ctx: Context):
        """Commands to list existing elements such as languages or tasks."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd list"))

    @list.command(name="languages", cls=BerziCommand, meta={"mod_only": True})
    async def list_languages(self, ctx, *, search: str = None):
        """List existing languages for Thing of the Day.

        :param ctx: the context.
        :param search: an optional string (case insensitive) to search for.
        """

        async with ctx.typing():
            if search:
                search = search.lower()

                results = await ctx.db.fetch(
                    "SELECT name FROM totd_languages"
                    " WHERE name ILIKE '%'|| $1 || '%';",
                    search,
                )
            else:
                results = await ctx.db.fetch("SELECT name FROM totd_languages;")

        if not results:
            extra = " matching" if search else ""
            await ctx.send(fetch_line("fail find language", extra))
            return

        result_string = "\n".join(result["name"] for result in results)

        await ctx.send(result_string)

    @list.command(name="tasks", cls=BerziCommand)
    async def list_tasks(
        self,
        ctx: Context,
        channel: Optional[discord.TextChannel] = None,
        *,
        language: str = None,
    ):
        """List Thing of the Day services.

        :param ctx: the context.
        :param channel: an optional channel to narrow down the tasks to list.
        :param language: an optional language to narrow down the tasks to list.
        """

        channel = channel or ctx.channel

        query_string, query_values = self._build_query(
            ctx, **{"channel": channel, "language": language,}
        )

        async with ctx.typing():
            results = await ctx.db.fetch(
                "SELECT"
                " l.name AS language,"
                " send_at AS time,"
                " server,"
                " channel,"
                " is_active AS active,"
                " t.created_at AS created"
                " FROM totd_tasks t JOIN totd_languages l ON l.id=t.language_id"
                f" WHERE {query_string};",
                *query_values,
            )

        if not results:
            await ctx.send(fetch_line("no match"))
            return

        # Preprocess items
        transformer = {
            "language": lambda x: x.capitalize(),
            "time": lambda x: x.strftime("%H:%M"),
            "channel": lambda x: ctx.guild.get_channel(x).name,
            "active": lambda x: "Yes" if x else "No",
            "created": lambda x: x.strftime("%Y-%m-%d %H:%M %Z"),
        }

        # Determine column headers and ordering
        headers = {
            "language": "Language",
            "time": "Time (server)",
            "channel": "Channel",
            "active": "Active",
            "created": "Created",
        }

        records = [headers]
        records.extend(
            [
                {
                    k: transformer.get(k, lambda x: str(x))(v)
                    for k, v in r.items()
                    if k in headers.keys()
                }
                for r in results
            ]
        )

        # Calculate max width of each column
        widths = {}
        for field, header in headers.items():
            widths[field] = max(
                reduce(
                    lambda x, y: max(len(str(x)), len(str(y))),
                    map(lambda r: r[field], records),
                ),
                len(header),
            )

        # Pad columns
        records = [
            {k: v.ljust(widths.get(k, 0)) for k, v in r.items()} for r in records
        ]

        padding_amount = 3
        padding = " " * padding_amount

        lines = [padding.join(record.values()) for record in records]
        output = "\n".join(lines)
        await ctx.send(f"```\n{output}```")

    @has_permissions(manage_channels=True)
    @thing_of_the_day.group(
        name="add", hidden=True, cls=BerziGroup, meta={"mod_only": True}
    )
    async def add(self, ctx: Context):
        """Commands to add new elements such as languages or entries."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd add"))

    @is_owner()
    @add.command(name="language", hidden=True, cls=BerziCommand)
    async def add_language(self, ctx, *, language: clean_content):
        """Add a language to which entries can be added."""
        language = language.lower()

        existing = await ctx.db.fetch(
            "SELECT * FROM totd_languages WHERE name=$1;", language
        )

        if existing:
            await ctx.send(fetch_line("already exists", language.capitalize()))
            return

        await ctx.db.execute("INSERT INTO totd_languages(name) VALUES($1);", language)

        await ctx.send(fetch_line("successful"))

        logger.info(f"{ctx.author} added {language=}.")

    @add.command(
        name="entry", alias=("entries",), cls=BerziCommand, meta={"mod_only": True}
    )
    async def add_entry(
        self, ctx, language: Optional[str] = None, *, input_string: clean_content = None
    ):
        """Add an entry to a language. Empty arguments, are prompted interactively.

        :param ctx: the context.
        :param language: the language to which to add the new entry or entries.
        :param input_string: the entry or entries (one per line). Format:
                              qualifier|content(|extra)
        """

        if not language:
            try:
                async with ctx.ask(fetch_line("what language")) as answer:
                    language = answer
            except asyncio.exceptions.TimeoutError:
                await ctx.send(fetch_line("cancelled"))
                return

        if "|" in language:
            # User inputted entry before language.
            raise UserInputError

        language = language.lower()
        language_id = await ctx.db.fetchval(
            "SELECT id FROM totd_languages WHERE name=$1;", language
        )

        if not language_id:
            await ctx.send(fetch_line("fail find", language.capitalize()))
            return

        if not input_string:
            message = (
                "And what do I add?\n"
                "*The format is `qualifier|content(|extra)`, one per line.*"
            )
            try:
                async with ctx.ask(message) as answer:
                    input_string = answer
            except asyncio.exceptions.TimeoutError:
                await ctx.send(fetch_line("cancelled"))
                return

        lines = input_string.split("\n")
        dupes = 0
        async with ctx.typing():
            existing_entries = await ctx.db.fetch(f"SELECT content FROM totd_entries;")
            existing_entries = {r[0] for r in existing_entries}
            entries = []
            for line in lines:
                entry = line.split("|", maxsplit=2)
                if entry[1] in existing_entries:
                    dupes += 1
                    continue

                if not entry[2:]:
                    entry.insert(2, "")
                entries.append(entry)

            await ctx.db.copy_records_to_table(
                "totd_entries",
                records=[(*entry, language_id, ctx.author.id) for entry in entries],
                columns=("qualifier", "content", "extra", "language_id", "author"),
            )

        if (n_entries := len(entries)) == 1:
            await ctx.send(fetch_line("successful"))
        else:
            await ctx.send(
                f"Added {n_entries} entries"
                f"{f' (I skipped {dupes} duplicates)' if dupes else ''}."
            )
        logger.info(f"{ctx.author} added {n_entries=} to {language_id=}.")

    @is_owner()
    @thing_of_the_day.group(name="remove", cls=BerziGroup, hidden=True)
    async def remove(self, ctx: Context):
        """Commands to remove elements such as languages or entries."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd remove"))

    @remove.command(name="language", hidden=True, cls=BerziCommand)
    async def remove_language(self, ctx, *, language: clean_content):
        """Remove a given language and all of its entries."""

        language = language.lower()

        language_id = await ctx.db.fetchval(
            "SELECT id FROM totd_languages WHERE name=$1;", language
        )

        if not language_id:
            await ctx.send(fetch_line("unknown", language.capitalize()))
            return

        message = fetch_line("remove language", language.capitalize())
        async with ctx.ask(message, boolean=True) as confirm:
            if not confirm:
                await ctx.send(fetch_line("cancelled"))
                return

        await ctx.db.execute("DELETE FROM totd_languages WHERE id=$1;", language_id)

        await ctx.send(fetch_line("successful"))

        logger.info(f"{ctx.author} removed {language=}.")

    @remove.command(name="entry", hidden=True, cls=BerziCommand)
    async def remove_entry(self, ctx, *, entry_content: clean_content):
        """Remove an entry from its language.

        :param ctx: the context.
        :param entry_content: the entry to remove.
        """

        if not entry_content:
            await ctx.send(fetch_line("need entry name"))
            return

        async with ctx.typing():
            entries = await ctx.db.fetch(
                "SELECT id, l.name AS language FROM totd_entries"
                " WHERE content ILIKE '%' || $1 || '%'"
                " INNER JOIN totd_languages l"
                " ON totd_entries.language_id=l.id;",
                entry_content,
            )

        if not entries:
            await ctx.send(fetch_line("fail find", entry_content))
            return

        to_remove = []
        if len(entries) > 1:
            message = (
                f"I found {entry_content} in multiple languages: "
                f"{', '.join(entry['language'] for entry in entries)}.\n"
                "Which do I remove?"
            )

            try:
                async with ctx.ask(message).lower() as answer:
                    if answer == "all":
                        to_remove = [entry["id"] for entry in entries]
                    else:
                        answer = [s.lower() for s in answer.split(",")]
                        for entry in entries:
                            if entry["language"] in answer:
                                to_remove.append(entry["id"])
            except asyncio.exceptions.TimeoutError:
                await ctx.send(fetch_line("cancelled"))
                return
        else:
            to_remove = [entries[0]["id"]]

        async with ctx.ask(fetch_line("sure"), boolean=True) as answer:
            if not answer:
                await ctx.send(fetch_line("cancelled"))
                return

        async with ctx.typing():
            await ctx.db.execute(
                "DELETE FROM totd_entries WHERE id=ANY($1::INT[]);", to_remove
            )

        await ctx.send(fetch_line("successful"))
        logger.info(f"{ctx.author} removed entries {to_remove}.")

    @is_owner()
    @thing_of_the_day.group(name="edit", hidden=True, cls=BerziGroup)
    async def edit(self, ctx: Context):
        """Commands to modify elements such as languages."""
        if ctx.invoked_subcommand is None:
            await ctx.send(fetch_line("invalid subcommand", "totd edit"))

    @edit.command(name="language", hidden=True, cls=BerziCommand)
    async def edit_language(self, ctx: Context, language: str, *, new_name: str):
        """Edit the name of a given language."""
        language = language.lower()
        new_name = new_name.lower()

        await ctx.db.execute(
            "UPDATE totd_languages SET name=$1 WHERE name=$2", new_name, language
        )

        await ctx.send(fetch_line("successful"))
        logger.info(f"{ctx.author} changed the name of {language=} to {new_name=}.")
