with open("TOKEN", "r") as token_file:
    TOKEN = token_file.read().strip()

DEBUG = True

LANGUAGE = "eng"

DEFAULT_DELETE_AFTER = 3.0
"""Default time to keep the bot's response after commands where it's desirable to delete it"""

PAGE_SIZE = 4
"""Default number of elements per page for pagination purposes."""

DEFAULT_TIMEOUT = 30.0
"""Default timeout in seconds for interactive elements."""

RECENT_COMMAND_TIME = DEFAULT_TIMEOUT
"""Number of seconds to keep recent commands cached for use with chain keywords."""
