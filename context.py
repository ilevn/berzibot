import asyncio
from contextlib import asynccontextmanager
from typing import Union
from discord.ext import commands
from locale import fetch_set


class _ContextDBAcquire:
    __slots__ = ("ctx", "timeout")

    def __init__(self, ctx, timeout):
        self.ctx = ctx
        self.timeout = timeout

    # noinspection PyProtectedMember
    def __await__(self):
        return self.ctx._acquire(self.timeout).__await__()

    # noinspection PyProtectedMember
    async def __aenter__(self):
        await self.ctx._acquire(self.timeout)
        return self.ctx.db

    async def __aexit__(self, *args):
        await self.ctx.release()


class BerziContext(commands.Context):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.pool = self.bot.pool
        self.db = None

    async def _acquire(self, timeout):
        if self.db is None:
            self.db = await self.pool.acquire(timeout=timeout)
        return self.db

    def acquire(self, *, timeout=None):
        """Acquires a database connection from the pool. e.g. ::
            async with ctx.acquire():
                await ctx.db.execute(...)
        or: ::
            await ctx.acquire()
            try:
                await ctx.db.execute(...)
            finally:
                await ctx.release()
        """
        return _ContextDBAcquire(self, timeout)

    async def release(self):
        """Releases the database connection from the pool.
        Useful if needed for "long" interactive commands where
        we want to release the connection and re-acquire later.
        Otherwise, this is called automatically by the bot.
        """

        if self.db is not None:
            await self.bot.pool.release(self.db)
            self.db = None

    @asynccontextmanager
    async def ask(
        self, message: str, *, boolean: bool = False, timeout: float = 30.0,
    ) -> Union[str, bool]:
        """Ask the user for input before continuing an operation.

        :param message: the question to ask the user.
        :param boolean: whether the answer should simply be either positive or negative.
        :param timeout: timeout in seconds. asyncio.exceptions.TimeoutError is raised if the prompt times
                         out. If boolean is True, False is returned instead.
        :return: the user's response, or, if boolean is True, True for positive and
                  False for negative responses.
        """

        await self.send(message)

        try:
            answer = await self.bot.wait_for(
                "message",
                check=lambda m: m.channel == self.channel and m.author == self.author,
                timeout=timeout,
            )
        except asyncio.exceptions.TimeoutError:
            if boolean:
                yield False
                return
            else:
                raise

        answer = answer.content

        if boolean:
            yield answer in fetch_set("positive response")
        else:
            yield answer.strip()
