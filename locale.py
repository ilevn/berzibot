try:
    from config import LANGUAGE
except ImportError:
    LANGUAGE = "eng"
import random
from typing import Dict, Set
from log import logger


locales: Dict[str, Dict[str, Set[str]]] = {
    "eng": {
        "positive response": {
            "yes",
            "y",
            "aye",
            "yeah",
            "ye",
            "yeh",
            "yh",
            "sure",
            "of course",
            "course",
            "ofc",
            "obviously",
            "obv",
            "affirmative",
            "naturally",
            "certainly",
            "definitely",
            "mhm",
            *"👍👌🙏🍆💯⭕✅🆗✔☑",
        },
        "negative response": {
            "no",
            "n",
            "nope",
            "nop",
            "nah",
            "nay",
            "negative",
            *"👎✋🖐💩❌🚫🚮✖",
        },
        "invalid subcommand": {"Invalid subcommand for `{}`, you baka."},
        "successful": {"Done. :>", "Alright!", "It is done. ù.ú", "👍"},
        "fail load": {"I couldn't load {}. :<"},
        "fail find": {"I couldn't find {}. :<"},
        "cogception": {"I can't unload the cog I use to load cogs... :/"},
        "unknown": {"I don't know {}!"},
        "unknown should": {"I don't know {}, should I?"},
        "cancelled": {
            "Nevermind then. :>",
            "Alright then...",
            "Guess I'll go back to my things...",
        },
        "task active": {"There already is an identical task active here!"},
        "task at for": {
            "There is a task at {} for {} already, did you mean to enable it?"
        },
        "fail find language": {"I can't find any{} language. :<"},
        "no match": {
            "I got nothing. :<",
            "Couldn't find anything. :<",
            "Doesn't ring any bells...",
        },
        "already exists": {"{} already exists!"},
        "what language": {"For what language?"},
        "remove language": {
            "Are you sure you want to remove {} **and all its entries**?",
        },
        "need entry name": {"I need a name. Reference the entry by the main word."},
        "sure": {"Are you sure?"},
        "fail": {
            'Oops, I messed something up. >.<"',
            "I done goofed! >.<",
            "Whelp, something didn't work...",
        },
        "only owner": {"I only answer to my master. ù.u", "What if berzi finds out?"},
        "insufficient permissions": {
            "You can't force me. :>",
            "Make me. :>",
            "How about no. :P",
            "Nah...",
        },
        "user error": {
            "You got something wrong...",
            "Can you double-check your input?",
            "Hmm, you sure about what you wrote?",
        },
        "listening to": {"your pings. ;)"},
        "logout": {
            "Logging out! o7",
            "RIP me. :<",
            "I'm ded.",
            "👋",
            "See you, space cowboy!",
        },
        "too long": {
            "That's too much content!",
            "Your message is too long.",
            "I can't handle so much stuff!",
        },
        "instructions": {
            "Ping me with a command. Write the (sub)command name after `help` for specific help.",
        },
        "no help": {"No help available."},
        "playing dead": {
            "dead..."
        },  # Shown after "Playing" in status when in sleep mode.
        "sleeping": {
            "Shh, I'm trying to sleep here!",
            "Only my prince can wake me up.",
            "Shh, berzi said I must sleep now.",
        },
        "goodnight": {"Good night... 🌃", "Good night! 🛌", "Going to sleep now. 👋"},
        "wake up": {"Hello world!", "Fresh as a rose. 🌹", "Yaaawn! 🥱", "🥱"},
        "totd footer": {
            "You can submit your own findings and TILs here, just post them!",
            "Learned something interesting recently? Post it here!",
            "Leave a reaction if you appreciate Thing of the Day!",
            "Remember that you're free to ask about the thing of the day! (Just, not here)",
            "Want to know more about this thing of the day? Ask in the appropriate channel!",
        },
    },
}


def fetch_line(code: str, *args) -> str:
    """Fetch a random line for the given code. If applicable, args are substituted into the line.
    
    If a code is not found, it is returned.
    """

    lines = tuple(locales[LANGUAGE].get(code, code))

    if not lines:
        logger.warning(f"{code=} in {LANGUAGE=} has no entries.")
        lines = ("",)

    line = random.choice(lines)

    if line == code:
        logger.warning(f"{code=} not found in {LANGUAGE=}")

    for arg in args:
        line = line.replace("{}", str(arg), 1)

    return line


def fetch_set(code: str) -> set:
    """Fetch the set of possible lines for the given code. No argument substitution is made."""
    return locales[LANGUAGE][code].copy()
