import logging
import sys

try:
    from config import DEBUG
except ImportError:
    DEBUG = False


FORMATTER = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s")

logger = logging.getLogger("discord.file")
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler(filename="discord.log", encoding="utf-8", mode="w")
file_handler.setFormatter(FORMATTER)
file_handler.setLevel(logging.DEBUG)

console_handler = logging.StreamHandler(stream=sys.stdout)
console_handler.setFormatter(FORMATTER)
if DEBUG:
    console_handler.setLevel(logging.DEBUG)
else:
    console_handler.setLevel(logging.WARNING)

logger.addHandler(file_handler)
logger.addHandler(console_handler)
