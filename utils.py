import asyncio
import datetime
import re
from typing import Optional, Callable, Sequence, Tuple, Awaitable, Dict

import discord

from config import PAGE_SIZE, DEFAULT_TIMEOUT
from context import BerziContext

capital = re.compile(r"(\w+?) *(?=[A-Z]|\b)")
EMOJI = {
    "q": "🇶",
    "w": "🇼",
    "e": "🇪",
    "r": "🇷",
    "t": "🇹",
    "y": "🇾",
    "u": "🇺",
    "i": "🇮",
    "o": "🇴",
    "p": "🇵",
    "a": "🇦",
    "s": "🇸",
    "d": "🇩",
    "f": "🇫",
    "g": "🇬",
    "h": "🇭",
    "j": "🇯",
    "k": "🇰",
    "l": "🇱",
    "z": "🇿",
    "x": "🇽",
    "c": "🇨",
    "v": "🇻",
    "b": "🇧",
    "n": "🇳",
    "m": "🇲",
    "1": "1️⃣",
    "2": "2️⃣",
    "3": "3️⃣",
    "4": "4️⃣",
    "5": "5️⃣",
    "6": "6️⃣",
    "7": "7️⃣",
    "8": "8️⃣",
    "9": "9️⃣",
    "0": "0️⃣",
}
NON_EMOJI = {v: k for k, v in EMOJI.items()}


def to_emoji_case(string: str) -> str:
    """Convert letters (ascii) and numbers to emoji."""
    return "".join(EMOJI.get(ch.lower(), ch) for ch in string)


def from_emoji_case(string: str) -> str:
    """Convert emoji to letters and numbers."""
    return "".join(NON_EMOJI.get(ch, ch) for ch in string)


def to_snake_case(string: str) -> str:
    """Convert a string to snake_case."""
    return capital.sub(r"\1_", string).lower().rstrip("_")


def from_snake_case(string: str) -> str:
    """Convert a string from snake_case to lower case."""
    middle = string[1:-1].replace("_", " ")
    return f"{string[0]}{middle}{string[-1]}"


def time_parts_from_string(string: str) -> Dict[str, int]:
    """Convert a human-language string to the equivalent in time parts as seen in datetime.timedelta arguments."""
    string = string.lower()
    parts = {}

    # First element of a descriptor must match an argument name of timedelta.
    descriptors = (
        ("weeks", "week", "w"),
        ("days", "day", "d"),
        ("hours", "hour", "h"),
        ("minutes", "minute", "m"),
        ("seconds", "second", "s"),
        ("milliseconds", "millisecond", "ms"),
        ("microseconds", "microsecond", "μs", "us"),
    )

    match = re.match(r"(\d+)\s(\w+)", string.strip())
    if not match:
        raise ValueError(f"Nothing got matched for string '{string}'.")

    for descriptor in descriptors:
        if match[2] in descriptor:
            parts[descriptor[0]] = int(match[1])

    if not parts:
        raise ValueError(f"No fitting descriptor found for '{string}'")

    return parts


def time_converter(argument: str) -> datetime.time:
    """Convert a textual time such as 23:41 to a time object with hour and minute."""
    this_tz = datetime.datetime.utcnow().astimezone().tzinfo

    if argument.lower() == "now":
        time = datetime.datetime.now().time().replace(tzinfo=this_tz)
    elif argument.lower().startswith("in "):
        time_parts = time_parts_from_string(argument.lstrip("in "))

        time = (
            (datetime.datetime.now() + datetime.timedelta(**time_parts))
            .time()
            .replace(tzinfo=this_tz)
        )
    else:
        time = (
            datetime.datetime.strptime(argument, "%H:%M")
            .time()
            .replace(tzinfo=datetime.timezone.utc)
        )

    return time


def current_timeofday() -> datetime.time:
    """Return the current time of day including only hours and minutes."""
    tzinfo = datetime.timezone.utc

    return (
        datetime.datetime.now(tzinfo)
        .time()
        .replace(second=0, microsecond=0, tzinfo=tzinfo)
    )


class EmbedPaginator:
    # Thanks to nully for help with this.
    _forward_emoji = "▶️"
    _backward_emoji = "◀️"

    def __init__(
        self,
        ctx: BerziContext,
        entries: Sequence[Tuple[str, str]],
        *,
        page_size=PAGE_SIZE,
        timeout=DEFAULT_TIMEOUT,
        thumbnail: str = None,
        title: str = None,
        description: str = None,
        author=False,
    ):
        self._bot = ctx.bot
        self._message = ctx.message
        self._channel = ctx.channel
        self._author = ctx.author
        self._embed = discord.Embed(colour=self._author.colour)
        if thumbnail:
            self._embed.set_thumbnail(url=thumbnail)
        if title:
            self._embed.title = title
        if description:
            self._embed.description = description
        if author:
            self._embed.set_author(
                name=self._author.name, icon_url=self._author.avater_url
            )

        self.entries = entries
        # Calculate how many pages are needed.
        self.current_page = 0
        self.page_size = page_size
        pages, left_over = divmod(len(self.entries), self.page_size)
        if left_over:
            pages += 1
        self.maximum_pages = pages
        self.timeout = timeout

        self._paginating = len(entries) > self.page_size
        # List of reactions with their respective functions.
        # Order matters! It matches the order of appearance under the post.
        self._reaction_emojis = [
            (self._backward_emoji, self.previous_page),
            (self._forward_emoji, self.next_page),
        ]

        self._match: Optional[Callable[[None], Awaitable[None]]] = None

    async def paginate(self):
        """Actually paginate the entries and run the interactive loop if necessary."""
        first_page = self._show_page(1, first=True)
        if not self._paginating:
            await first_page
        else:
            # Allow us to react to reactions right away if we're paginating.
            self._bot.loop.create_task(first_page)

        while self._paginating:
            try:
                reaction, user = await self._bot.wait_for(
                    "reaction_add", check=self._react_check, timeout=self.timeout
                )
            except asyncio.exceptions.TimeoutError:
                self._paginating = False
                try:
                    # Clear all reactions upon exit.
                    await self._message.clear_reactions()
                except discord.HTTPException:
                    pass
                finally:
                    break

            try:
                await self._message.remove_reaction(reaction, user)
            except discord.HTTPException:
                # Can't remove it so don't bother doing so.
                pass

            await self._match()

    async def next_page(self):
        """Flip to the next page."""
        await self._checked_show_page(self.current_page + 1)

    async def previous_page(self):
        """Flip to the previous page."""
        await self._checked_show_page(self.current_page - 1)

    def _get_page(self, page: int):
        base = (page - 1) * self.page_size
        # Quick maffs gets us the next message chunk for our page.
        return self.entries[base: base + self.page_size]

    def _flip_to_page(self, entries: Sequence[Tuple[str, str]], page: int):
        self._embed.clear_fields()

        for key, value in entries:
            self._embed.add_field(name=key, value=value, inline=False)

        if self._paginating:
            page_count = (
                f"Page {to_emoji_case(str(page))} of"
                f" {to_emoji_case(str(self.maximum_pages))}"
                if self.maximum_pages > 1
                else ""
            )
            info = (
                f"React with"
                f" {''.join((self._backward_emoji, self._forward_emoji))}"
                f" to flip page or write the"
                f" desired page name in chat within {int(self.timeout)} seconds."
            )

            self._embed.set_footer(
                text=f"{page_count}{info}",
                # Information icon from twitter's emoji.
                icon_url="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com"
                "/thumbs/120/twitter/233/information-source_2139.png",
            )

    async def _checked_show_page(self, page: int):
        if page != 0 and page <= self.maximum_pages:
            await self._show_page(page)

    async def _show_page(self, page: int, *, first=False):
        self.current_page = page
        entries = self._get_page(page)
        self._flip_to_page(entries, page)

        if not self._paginating:
            # Set last message. This allows us to delete it more easily.
            self._message = await self._channel.send(embed=self._embed)
            return self._message

        if not first:
            # Message exists, edit instead of posting another one.
            await self._message.edit(embed=self._embed)
            return

        self._message = await self._channel.send(embed=self._embed)
        for (reaction, _) in self._reaction_emojis:
            # Assign our reactions.
            await self._message.add_reaction(reaction)

    def _react_check(self, reaction: discord.Reaction, user: discord.User):
        if user is None or user.id != self._author.id:
            return False

        if reaction.message.id != self._message.id:
            return False

        for (emoji, func) in self._reaction_emojis:
            if reaction.emoji == emoji:
                # Cheeky function assignment.
                self._match = func
                return True

        return False
